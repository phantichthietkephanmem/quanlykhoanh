<div class="form">


     <ul class="tab-group">
        <li class="tab  active"><a href="#login">Log In</a></li>
        <li class="tab"><a href="#signup">Sign Up</a></li>
        
      </ul>


      <div class="tab-content">

        
        <div id="login">   
          <h1>Welcome Back!</h1>
          
          <?php $this->render('cmswidgets.views.notification_frontend'); ?>
          <?php $form=$this->beginWidget('CActiveForm', array(
               'id'=>'login-content',
          'enableClientValidation'=>true,
          'clientOptions'=>array(
            'validateOnSubmit'=>true,
          ),     
                )); 
            ?>
          
            <div class="field-wrap">
              <label>
                Email Address<span class="req">*</span>
              </label>
              <!--<input type="email"required autocomplete="off"/>-->
              <?php echo $form->textField($model,'username',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
              <?php echo $form->error($model,'username'); ?>
            </div>
          
            <div class="field-wrap">
              <label>
                Password<span class="req">*</span>
              </label>
              <!--<input type="password"required autocomplete="off"/>-->
               <?php echo $form->passwordField($model,'password',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
               <?php echo $form->error($model,'password'); ?>
            </div>
          
          <p class="<?php echo bu();?>/forgot-password"><a href="#">Forgot Password?</a></p>
          
          <button class="button button-block btn primary" id="bSigninButton"/>Log In</button>
          <?php $this->endWidget(); ?>

        </div>
 
                <div id="signup">   
          <h1>Sign Up for Free</h1>
          
          <form action="/" method="post">

            <div class="top-row">
              <div class="field-wrap">
                <label>
                  First Name<span class="req">*</span>
                </label>
                <input type="text" required autocomplete="off" />
              </div>

              <div class="field-wrap">
                <label>
                  Last Name<span class="req">*</span>
                </label>
                <input type="text"required autocomplete="off"/>
              </div>
            </div>

            <div class="field-wrap">
              <label>
                Email Address<span class="req">*</span>
              </label>
              <input type="email"required autocomplete="off"/>
            </div>

            <div class="field-wrap">
              <label>
                Set A Password<span class="req">*</span>
              </label>
              <input type="password"required autocomplete="off"/>
            </div>

            <button type="submit" class="button button-block"/>Get Started</button>

          </form>

        </div>
      </div>
  
</div>