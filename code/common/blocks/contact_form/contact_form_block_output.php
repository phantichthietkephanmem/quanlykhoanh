<div id="contactFormContainer">
	<div id="contactForm">
		<fieldset>
			<label for="Name">Name *</label>
			<input id="name" type="text" />
			<label for="Email">Email address *</label>
			<input id="Email" type="text" />
			<label for="Message">Your message *</label>
			<textarea id="Message" rows="3" cols="20"></textarea>
			<input id="sendMail" type="submit" name="submit" onClick="closeForm()" />
			<span id="messageSent">Your message has been sent successfully!</span>
		</fieldset>
	</div>
	<div id="contactLink"></div>
</div>