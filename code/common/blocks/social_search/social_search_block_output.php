<div class="header">
	<!--<div class="social-icons">						
		<ul>
			<li><a class="facebook" href="#" target="_blank"> </a></li>
			<li><a class="twitter" href="#" target="_blank"></a></li>
			<li><a class="googleplus" href="#" target="_blank"></a></li>
			<li><a class="pinterest" href="#" target="_blank"></a></li>
			<li><a class="dribbble" href="#" target="_blank"></a></li>
			<li><a class="vimeo" href="#" target="_blank"></a></li>
			<div class="clear"></div>
		</ul>
	</div>-->
	<div class="social-icons">
		<div class="div-upload">
			<a class="upload-button" href="https://unsplash.com/submit" class="">Submit a photo</a>  
		</div>
			<?php if(!user()->isGuest) {?>
				<a href ="" class="login-button"><?= user()->getModel('display_name') ?></a> -
				<a href="<?=SITE_PATH?>/site/logout" class="login-button"> Logout</a>
			<?php	}else{ ?>

				<a href="<?= SITE_PATH?>page?slug=signup-login" rel="nofollow" class="login-button">Login</a>
			<?php } ?>
	</div>
	<div class="search_box">
		<form>
			<input type="text" class="text-box" placeholder="Search............."><input type="submit" value="">
		</form>
	</div>
	<div class="clear"></div>
</div>
<style type="text/css">
.upload-button{
	
	color: #999;
    display: inline-block;
    height: 36px;
    padding: 0 11px;
    font-size: 14px;
    line-height: 34px;
    cursor: pointer;
    border: 1px solid #ccc;
    border-radius: 5px;
    box-shadow: 0 1px 1px rgba(0,0,0,.04);
    transition: all .2s ease-in-out;
    text-align: center;
    text-decoration: none;
}
.upload-button:hover{
	color: #333;
	border-color:#333;
}
.div-upload{
	padding-right: 15px;
	display: inline-block;
	/*border-right: 1px solid #ddd; */
}
.login-button{
    color: #999;
    padding-left:10px;
    padding-right:8px;
    font-size: 14px;
}
.login-button:hover{
	color: #333;
}

</style>