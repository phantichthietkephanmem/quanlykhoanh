<?php $image = GxcHelpers::getThumbnail($post->object_id); ?>
<div class="content">
	<div class="box1">
		<h3><a href="details.html"><?php echo CHtml::encode($post->object_name); ?></a></h3>
		<span>By <?php echo  $post->object_author_name; ?>- <?php echo date("m/d/Y H:i", $post->object_date); ?><span class="comments">8 Comments</span></span> 
		<div class="blog-img">
			<div class="view-back">
				<span class="views" title="views">(566)</span>
				<span class="likes" title="likes">(124)</span>
			</div>
			<img src="<?= $image['link']; ?>">
		</div>
		<div class="blog-data">
			<?php echo $post->object_content; ?>
		</div>
		<div class="clear"></div>
        <div class="fb-like" data-href="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" data-layout="button" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
    </div>

	<!----------------  Comment Area -------------------->
	<div class="comments-area">
		<h3>Leave a comment</h3>
        <div class="fb-comments" data-href="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" data-numposts="5"></div>
</div>
<div class="clear"> </div>