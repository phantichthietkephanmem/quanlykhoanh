<?php

  $link = $data->getObjectLink();
  $image = GxcHelpers::getThumbnail($data->object_id);
?>
<li data-filter-class='["photos", "blog"]'>
	<a href="<?= $image['link']; ?>" class="swipebox" title="Image Name"> 
		<img  src="<?= $image['link']; ?>" alt="" />
	</a>
	<p><a href="<?= $link; ?>">
		<img src="<?= $asset; ?>/images/blog-icon1.png" title="posted date" alt="" />
		<img src="<?= $asset; ?>/images/blog-icon2.png" title="views" alt="" />
		<img src="<?= $asset; ?>/images/blog-icon3.png" title="comments" alt="" />
		<img src="<?= $asset; ?>/images/blog-icon5.png" title="link" alt="" />
		<span><?= CHtml::encode($data->object_name); ?></span>
		<div class="clear"></div>
	</a></p>
</li>
