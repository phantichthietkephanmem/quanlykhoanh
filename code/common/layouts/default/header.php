<!DOCTYPE HTML>
<html>
<head>
<title><?php echo $this->pageTitle; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="<?php echo $layout_asset; ?>/css/style.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo $layout_asset; ?>/css/grids.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo $layout_asset; ?>/css/swipebox.css">
<?php    
    $cs=Yii::app()->clientScript;
    $cs->registerScriptFile( $layout_asset.'/js/jquery.min.js', CClientScript::POS_HEAD);        
    $cs->registerScriptFile( $layout_asset.'/js/ios-orientationchange-fix.js', CClientScript::POS_HEAD);  
    $cs->registerScriptFile( $layout_asset.'/js/jquery.swipebox.min.js', CClientScript::POS_HEAD);

    Yii::app()->clientScript->registerScript('swipebox', '
        jQuery(function($) {
            $(".swipebox").swipebox();
        });
    ', CClientScript::POS_HEAD);

    Yii::app()->clientScript->registerScript('contactlink', '
        $(document).ready(function(){

            $("#contactLink").click(function(){
                if ($("#contactForm").is(":hidden")){
                    $("#contactForm").slideDown("slow");
                }
                else{
                    $("#contactForm").slideUp("slow");
                }
            });
            
        });
        
        function closeForm(){
            $("#messageSent").show("slow");
            setTimeout(\'$("#messageSent").hide();$("#contactForm").slideUp("slow")\', 2000);
       }
    ', CClientScript::POS_HEAD);


    $cs->registerScriptFile($layout_asset . '/js/jquery.imagesloaded.js', CClientScript::POS_END);
    $cs->registerScriptFile($layout_asset . '/js/jquery.wookmark.js', CClientScript::POS_END);

    Yii::app()->clientScript->registerScript('imagesloaded', '
    (function ($){
      $(\'#tiles\').imagesLoaded(function() {
        // Prepare layout options.
        var options = {
          autoResize: true, // This will auto-update the layout when the browser window is resized.
          container: $(\'#main\'), // Optional, used for some extra CSS styling
          offset: 2, // Optional, the distance between grid items
          itemWidth:310 // Optional, the width of a grid item
        };

        // Get a reference to your grid items.
        var handler = $(\'#tiles li\'),
            filters = $(\'#filters li\');

        // Call the layout function.
        handler.wookmark(options);

        /**
         * When a filter is clicked, toggle its active state and refresh.
         */
        var onClickFilter = function(event) {
          var item = $(event.currentTarget),
              activeFilters = [];
          item.toggleClass(\'active\');

          // Collect active filter strings
          filters.filter(\'.active\').each(function() {
            activeFilters.push($(this).data(\'filter\'));
          });

          handler.wookmarkInstance.filter(activeFilters, \'or\');
        }

        // Capture filter click events.
        filters.click(onClickFilter);
      });
    })(jQuery);
    ', CClientScript::POS_END);
?>

<?php 
    //Render Widget for Header Script
    $this->widget('BlockRenderWidget',array('page'=>$page,'region'=>'3','layout_asset'=>$layout_asset)); 
?>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11&appId=144214729601573';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</head>
