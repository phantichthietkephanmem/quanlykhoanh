<?php
$layout_asset=GxcHelpers::publishAsset(Yii::getPathOfAlias('common.layouts.default.assets')); 	
?>
<?php $this->renderPartial('common.layouts.default.header',array('page'=>$page,'layout_asset'=>$layout_asset));?>      

<body>		

	<div class="main">
		<div class="wrap">
			<div class="left-content">
				<div class="logo">
					<h1><a href="<?= SITE_PATH ?>"><img src="<?php echo $layout_asset; ?>/images/logo.png" alt="" /></a></h1>
				</div>
				<?php $this->widget('BlockRenderWidget',array('page'=>$page,'region'=>'0','layout_asset'=>$layout_asset)); ?>
			</div>
			<?php $this->widget('BlockRenderWidget',array('page'=>$page,'region'=>'1','layout_asset'=>$layout_asset)); ?>
			<div class="right-content">
				<?php $this->widget('BlockRenderWidget',array('page'=>$page,'region'=>'2','layout_asset'=>$layout_asset)); ?>

				<div id="content">
					<div id="main" role="main">
						<ul id="tiles">
							<?php $this->widget('BlockRenderWidget',array('page'=>$page,'region'=>'3','layout_asset'=>$layout_asset)); ?>
					
						</ul>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>


</body>
</html>
