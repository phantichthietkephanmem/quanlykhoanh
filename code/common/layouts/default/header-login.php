<!DOCTYPE HTML>
<html>
<head>
<title><?php echo $this->pageTitle; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo $layout_asset; ?>/css/normalize.min.css">
<link rel="stylesheet" href="<?php echo $layout_asset; ?>/css/login.css">
<?php    
    $cs=Yii::app()->clientScript;
    $cs->registerScriptFile( $layout_asset.'/js/jquery-1.11.3.min.js', CClientScript::POS_END);    
    $cs->registerScriptFile( $layout_asset.'/js/login.js', CClientScript::POS_END);  
?>
</head>
