<?php
//echo "resize now"; exit;

require(__DIR__ . '/../vendor/autoload.php');

ini_set("memory_limit", "80M");

$config = [

    // Define Resource Folder
    // Remember to use the / at the end of the path
    'resource_folder' => dirname(__DIR__) . '/../resources/tto/r/',

    // Define Resource Folder
    // Remember to use the / at the end of the path
    'cache_folder' => dirname(__DIR__) . '/../resources/tto/i/',

    // Define allowed resize sizes
    'sizes' => [
        's1280' => ['id' => 's1280',
            'width' => 1280,
            'height' => 720,
            'name' => '1280x720',
            'ratio' => '1280x720',
            'keep_original_ratio' => true
        ],
        's480' => ['id' => 's480',
            'width' => 480,
            'height' => 270,
            'name' => '480x270',
            'ratio' => '480x270',
            'keep_original_ratio' => true
        ],
        's640' => ['id' => 's640',
            'width' => 640,
            'height' => 360,
            'name' => '640x360',
            'ratio' => '640x360',
            'keep_original_ratio' => false
        ],
        's626' => ['id' => 's626',
            'width' => 665,
            'height' => 391,
            'name' => '665x391',
            'ratio' => '665:391',
            'keep_original_ratio' => true
        ],
        's490' => ['id' => 's490',
            'width' => 490,
            'height' => 276,
            'name' => '490x276',
            'ratio' => '490x276',
            'keep_original_ratio' => false
        ],
        's440' => ['id' => 's440',
            'width' => 440,
            'height' => 249,
            'name' => '440x249',
            'ratio' => '440x249',
            'keep_original_ratio' => false
        ],
        's200' => ['id' => 's200',
            'width' => 200,
            'height' => 200,
            'name' => '200x200',
            'ratio' => '200:200',
            'keep_original_ratio' => false
        ],                
        's146' => ['id' => 's146',
            'width' => 146,
            'height' => 82,
            'name' => '146x82',
            'ratio' => '146:82',
            'keep_original_ratio' => false
        ],
        's141' => ['id' => 's141',
            'width' => 141,
            'height' => 210,
            'name' => '141x210',
            'ratio' => '141:210',
            'keep_original_ratio' => false
        ],         
        's140' => ['id' => 's140',
            'width' => 140,
            'height' => 140,
            'name' => '140x140',
            'ratio' => '140:140',
            'keep_original_ratio' => false
        ],        
    ],
];


// Now we start processing the Image
$request_path = isset($_GET['path']) ? $_GET['path'] : '';
$request_file = trim(strstr($request_path, '/'), '/');

$tokens = explode("/", $request_path);

$request_size = isset($tokens[0]) ? $tokens[0] : '';


// Do check file exist
$original_file = $config['resource_folder'] . $request_file;


if (!file_exists($original_file)) {
    header("Status: 404 Not Found");
    echo 'Not found 1;)';

    return 0;
}


// So we have the original file, we check the size allowed
if (!isset($config['sizes'][$request_size]) || !in_array($request_size, $config['sizes'][$request_size])) {
    header("Status: 404 Not Found");
    echo 'Not found 2;)';

    return 0;
}

// ok, allowed file, allowed size
// Nginx passes the request here, means that there is no caching version
// of this image in the folder, so we might want to resize it on the fly

# parse out the requested image dimensions and resize mode
$target_width = $config['sizes'][$request_size]['width'];
$target_height = $config['sizes'][$request_size]['height'];
$target_keep_ratio = $config['sizes'][$request_size]['keep_original_ratio'];
$target_ratio = $target_width / $target_height;


// $new_width = $target_width;
// $new_height = $target_height;
$image = new Imagick($original_file);

/* What if the source is vertical image */

//$background = preg_match('/\.gif$|\.png$/', $request_file) == 1 ? 'None' : 'white';

$orig_width = $image->getImageWidth();
$orig_height = $image->getImageHeight();
$orig_ratio = $orig_width / $orig_height;

$image->setImageCompressionQuality(85);

// Image resize in content
if ($target_keep_ratio) {
    if ($orig_width >= $target_width) {
        $image->scaleImage($target_width, $target_width / $orig_ratio);
    } else {
        $image->resizeImage($orig_width, $orig_height, imagick::FILTER_LANCZOS, 1);
    }
} else {

    // crop to get desired aspect ratio
    $desired_aspect = $target_width / $target_height;

    if ($desired_aspect > $orig_ratio) {
        $trim = $orig_height - ($orig_width / $desired_aspect);
        $image->cropImage($orig_width, $orig_height - $trim, 0, $trim / 4);
    } else {
        $trim = $orig_width - ($orig_height * $desired_aspect);
        $image->cropImage($orig_width - $trim, $orig_height, $trim / 2, 0);
    }

    $image->resizeImage($target_width, $target_height, imagick::FILTER_LANCZOS, 1);

}

// Setup folder path for caching
$save_cache_path = $config['cache_folder'] . $config['sizes'][$request_size]['id'] . '/' . $request_file;
$save_cache_dir = dirname($save_cache_path);

# save and return the resized image file
if (!file_exists($save_cache_dir))
    mkdir($save_cache_dir, 0777, true);

$image->writeImage($save_cache_path);
$imageFormat = strtolower($image->getimageformat());
header('Content-Type: image/' . $imageFormat);
header('Content-Length: ' . filesize($save_cache_path));
echo file_get_contents($save_cache_path);

return true;
